with open("data.txt", "r") as f:
  info = f.read().splitlines()

lsPairs = []
cpt = 0

# print(info)

for x in info:
    compare = x.split(",")
    pairs1 = compare[0].split("-")
    pairs2 = compare[1].split("-")
    if (int(pairs1[0]) <= int(pairs2[0]) and int(pairs1[1]) >= int(pairs2[1]))\
      or int(pairs1[0]) >= int(pairs2[0]) and int(pairs1[1]) <= int(pairs2[1]):
          cpt += 1

print(cpt)
