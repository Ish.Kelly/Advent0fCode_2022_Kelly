with open("data.txt", "r") as f:
    info = f.read().splitlines()

alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
score = 0

for x in info:
    mid = round(len(x) / 2)
    part1 = x[:mid]
    part2 = x[mid:]
    for y in part1:
        if y in part2:
            score += alphabet.index(y) + 1
            break
print(score)
