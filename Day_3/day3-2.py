with open("data.txt", "r") as f:
    info = f.read().splitlines()

alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
lsOf3 = []
score = 0
for x in info:
    lsOf3.append(x)
    if len(lsOf3) == 3:
        for char in lsOf3[0]:
            if char in lsOf3[1] and char in lsOf3[2]:
                score += alphabet.index(char) + 1
                break
        lsOf3 = []
print(score)
