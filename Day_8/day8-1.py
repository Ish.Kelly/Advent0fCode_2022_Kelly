with open("data.txt", "r") as f:
    info = f.read().splitlines()

treeVisible = 0
treeOnEdgeLR = len(info) * 2  # Nombre d'arbre visible droite gauche
treeOnEdgeHB = 0  # Nombre d'arbre visible haut bas

for cpt1 in range(0, len(info), 1):
    if len(info[cpt1]) > treeOnEdgeHB:
        treeOnEdgeHB = len(info[cpt1])

    for cpt2 in range(0, len(info[cpt1]), 1):
        nbBig = 0
        boVisible = False
        for x in range(0, len(info[cpt1]), 1):
            if nbBig < int(info[cpt1][x]):
                nbBig = int(info[cpt1][x])
            if x == cpt2 and nbBig < int(info[cpt1][cpt2]):
                boVisible = True
                break
            elif x == len(info[cpt1]) and nbBig < int(info[cpt1][cpt2]):
                boVisible = True
                break
        nbBig = 0
        if not boVisible:
            for cpt3 in range(0, len(info), 1):
                if nbBig < int(info[cpt3][cpt2]):
                    nbBig = int(info[cpt3][cpt2])
                if cpt3 == cpt1 and nbBig < int(info[cpt1][cpt2]):
                    boVisible = True
                    break
                elif cpt3 == len(info[cpt1]) and nbBig < int(info[cpt1][cpt2]):
                    boVisible = True
                    break

        if boVisible:
            treeVisible += 1

print(treeOnEdgeHB)
print(treeOnEdgeLR)
print(treeVisible)
print(treeVisible + treeOnEdgeLR + treeOnEdgeHB)
