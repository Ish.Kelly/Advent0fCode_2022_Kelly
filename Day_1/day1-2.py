with open("calorie.txt", "r") as f:
    info = f.read().splitlines()

mostCalorie1 = 0
mostCalorie2 = 0
mostCalorie3 = 0
compareCalorie = 0

for cpt in range(0, len(info), 1):
    if info[cpt] != "":
        compareCalorie += int(info[cpt])
    else:
        if compareCalorie > mostCalorie1:
            mostCalorie3 = mostCalorie2
            mostCalorie2 = mostCalorie1
            mostCalorie1 = compareCalorie
        elif compareCalorie > mostCalorie2:
            mostCalorie3 = mostCalorie2
            mostCalorie2 = compareCalorie
        elif compareCalorie > mostCalorie3:
            mostCalorie3 = compareCalorie
        compareCalorie = 0

print(mostCalorie1 + mostCalorie2 + mostCalorie3)
