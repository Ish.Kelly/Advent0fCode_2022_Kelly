with open("calorie.txt", "r") as f:
    info = f.read().splitlines()

mostCalorie = 0
compareCalorie = 0

for cpt in range(0, len(info), 1):
    if info[cpt] != "":
        compareCalorie += int(info[cpt])
    elif compareCalorie > mostCalorie:
        mostCalorie = compareCalorie
        compareCalorie = 0
    else:
        compareCalorie = 0

print(mostCalorie)
