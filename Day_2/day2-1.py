with open("data.txt", "r") as f:
    info = f.read().splitlines()

score = 0

lsA = ['A', 'B', 'C']
lsB = ['X', 'Y', 'Z']

def getPoint(x):
    if x == 'X':
        val = 1
    elif x == 'Y':
        val = 2
    else:
        val = 3
    return val

for x in info:
    dataSplit = x.split()

    for y in lsA:
        if y == dataSplit[0]:
            # gagne
            if lsB.index(dataSplit[1]) - lsA.index(dataSplit[0]) == -2 or lsB.index(dataSplit[1]) - lsA.index(dataSplit[0]) == 1:
                score += 6 + getPoint(dataSplit[1])

            # perd
            elif lsB.index(dataSplit[1]) - lsA.index(dataSplit[0]) == -1 or lsB.index(dataSplit[1]) - lsA.index(dataSplit[0]) == 2:
                score += 0 + getPoint(dataSplit[1])

            # egalite
            elif lsB.index(dataSplit[1]) == lsA.index(dataSplit[0]):
                score += 3 + getPoint(dataSplit[1])

print(score)
