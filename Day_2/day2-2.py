with open("data.txt", "r") as f:
    info = f.read().splitlines()

score = 0

lsA = ['A', 'B', 'C']
lsB = ['X', 'Y', 'Z']

def getPoint(x):
    if x == 'A':
        val = 1
    elif x == 'B':
        val = 2
    else:
        val = 3
    return val


for x in info:
    dataSplit = x.split()

    for y in lsA:
        if y == dataSplit[0]:
            # gagne
            if dataSplit[1] == 'Z':
                if lsA.index(dataSplit[0]) != 2:
                    score += 6 + getPoint(lsA[lsA.index(dataSplit[0]) + 1])
                else:
                    score += 6 + getPoint(lsA[lsA.index(dataSplit[0]) - 2])

            # perd
            elif dataSplit[1] == 'X':
                if lsA.index(dataSplit[0]) != 0:
                    score += 0 + getPoint(lsA[lsA.index(dataSplit[0]) - 1])
                else:
                    score += 0 + getPoint(lsA[lsA.index(dataSplit[0]) + 2])


            # egalite
            elif dataSplit[1] == 'Y':
                score += 3 + getPoint(dataSplit[0])

print(score)
